// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){
		$('.gform_body .ginput_container > input').focus(function(){
			$(this).parent('.ginput_container').addClass('triangle')
		})
		$('.gform_body .ginput_container > input').focusout(function(){
			$(this).parent('.ginput_container').removeClass('triangle')
		})
		$('.gform_body .ginput_container > textarea').focus(function(){
			$(this).parent('.ginput_container').addClass('triangle')
		})
		$('.gform_body .ginput_container > textarea').focusout(function(){
			$(this).parent('.ginput_container').removeClass('triangle')
		})
    });

}(jQuery));