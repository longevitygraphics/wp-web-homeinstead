import "../sass/style.scss";
require('./window-event/ready.js');
require('./window-event/load.js');
require('./window-event/resize.js');
require('./window-event/scroll.js');
require("../images/5star.png");


var SmoothScroll = require('smooth-scroll/src/js/smooth-scroll/smooth-scroll');

var scroll = new SmoothScroll('a[href*="#"]', {
  header: "#site-header"
});
