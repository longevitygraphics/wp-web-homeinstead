<?php
function add_child_template_support() {
	/*add theme colors*/
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Primary', 'wp-theme-parent' ),
			'slug'  => 'primary',
			'color' => '#681D49'
		),
		array(
			'name'  => __( 'Secondary', 'wp-theme-parent' ),
			'slug'  => 'secondary',
			'color' => '#998B39'
		),
		array(
			'name'  => __( 'Tertiary', 'wp-theme-parent' ),
			'slug'  => 'tertiary',
			'color' => '#5A5A5A'
		),
		array(
			'name'  => __( 'White', 'wp-theme-parent' ),
			'slug'  => 'white',
			'color' => '#ffffff'
		),
		array(
			'name'  => __( 'Dark', 'wp-theme-parent' ),
			'slug'  => 'dark',
			'color' => '#333333'
		),
		array(
			'name'  => __( 'BG', 'wp-theme-parent' ),
			'slug'  => 'bg',
			'color' => '#E8E1D5'
		)
	) );

	//add support for editor styles
	add_theme_support( "editor-styles" );
}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );
