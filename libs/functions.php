<?php

// Footer Charlie ~ First Item
register_sidebar(
	array(
		'name'          => esc_html__( 'Footer Delta', '_s' ),
		'id'            => 'footer-delta',
		'description'   => esc_html__( 'Add widgets here.', '_s' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	)
);

//add script to head
add_action('wp_head','lg_add_script_to_header', 100);
function lg_add_script_to_header() {
 echo '<script async src="//141519.tctm.co/t.js"></script>';
}

