<div class="main-navigation">
	<nav class="navbar navbar-expand-xl navbar-dark bg-primary">
		<a class="navbar-brand mr-auto mr-lg-0" href="#"><?php echo site_logo(); ?></a>

		<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button" data-toggle="offcanvas">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</button>

		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse bg-primary" id="main-navbar">
			<?php

			$mainMenu = array(
				// 'menu'              => 'menu-1',
				'theme_location' => 'top-nav',
				'depth'          => 2,
				'container'      => false,
				/*'container' => 'div',
				'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
				'container_id' => 'main-navbar',*/
				'menu_class'     => 'navbar-nav mr-auto',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			);
			wp_nav_menu( $mainMenu );

			?>
			<div class="header-right-content">
				<h5>
					<span>Call Us 24/7: </span>
					<a
						class="wp-block-button__link has-background has-secondary-background-color"
						href="tel:604-432-1139">604-432-1139</a>
				</h5>
			</div>
		</div>
	</nav>
</div>
